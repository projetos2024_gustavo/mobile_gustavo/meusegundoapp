//import Layout from './layoutDeTelaEstrutura';
//import LayoutHorizontal from './LayoutHorizontal'
//import LayoutGrade from './LayoutGrade';
//import Componentes from "./Component";a
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import TelaInicial from "./src/telaInicial";
import SegundaTela from "./src/segundaTela";
import Menu from "./src/component/menu";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu} /> 
        <Stack.Screen name="TelaInicial" component={TelaInicial} />
        <Stack.Screen name="SegundaTela" component={SegundaTela} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
