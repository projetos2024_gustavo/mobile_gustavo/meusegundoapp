import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
//import Layout from './layoutDeTelaEstrutura';
//import LayoutHorizontal from './LayoutHorizontal'
//import LayoutGrade from './LayoutGrade';
import Componentes from "./Component";

export default function Menu({ navigation }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("TelaInicial")}
      >
        <Text>Opição 1</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("SegundaTela")}
      >
        <Text>Opição 2</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}>
        <Text>Opição 3</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  menu: {
    padding: 10,
    margin: 5,
    backgroundColor: "lightblue",
    borderRadius: 5,
  },
});
