import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Button,
  } from "react-native";


function SegundaTela({ navigation }) {
    return (
      <View style={styles.container}>
        <Text>SegundaTela</Text>
        <Button title="Voltar Para Home" onPress={() => navigation.goBack()} />
      </View>
    );
  }
  export default SegundaTela;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
  });